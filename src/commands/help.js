const message =
`
Usage: short url [name] [options]

  ... without installing:
  $ npx @commonshost/short url [name] [options]

  ... installed locally to a project:
  $ npm install @commonshost/short
  $ npx short url [name] [options]

  ... installed globally on the system:
  $ npm install --global @commonshost/short
  $ short url [name] [options]

The name is optional. If omitted, a random sequence of decimals, hexadecimals,
or emoji is generated as the shortened URL pathname.

Options:

  url
  An absolute http: or https: URL. Be careful with query strings containing
  ampersants or hash fragments. Your shell may interpret them as operators.
  It is safest to wrap the URL in single or double quotes.

  name
  Custom string sequence for the shortened URL.
  Default: undefined

  --decimal, -d
  Generates a random decimal number sequence.
  Default: true

  --emoji, -e
  Generates a random emoji sequence.
  Default: false

  --hex, -x
  Generates a random hexadecimal sequence.
  Default: false

  --help, -h
  Print this message.

  --version, -v
  Print current package version.

Examples:

  $ short "https://example.com/very/long/url"
  Long:  https://example.com/very/long/url
  Short: https://mysite.example/5981

  $ short "https://example.com/very/long/url" --emoji
  Long:  https://example.com/very/long/url
  Short: https://mysite.example/🦄🌈

  $ short "https://example.com/very/long/url" --hex
  Long:  https://example.com/very/long/url
  Short: https://mysite.example/8e3b
`

console.log(message)
