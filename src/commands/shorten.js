const { promisify } = require('util')
const { readFile, writeFile } = require('fs')
const { findFile } = require('@commonshost/configuration')
const UriTemplate = require('uri-templates')
const { domainToUnicode } = require('url')

module.exports = async function shorten (options) {
  const cwd = process.cwd()

  const configurationPath = await findFile(cwd)
  if (!configurationPath) {
    console.error(`Error: No site configuration file found in: ${cwd}`)
    process.exit(1)
  }
  if (!configurationPath.endsWith('.json')) {
    console.error('Error: Only .json site configuration files are supported for now.')
    process.exit(1)
  }

  let name
  if (options.name) {
    name = options.name
  } else if (options.emoji) {
    const { random } = require('random-unicode-emoji')
    name = random({ count: 2 }).join('')
  } else if (options.hex) {
    const buffer = Buffer.alloc(2)
    name = require('crypto').randomFillSync(buffer).toString('hex')
  } else if (options.decimal) {
    name = String(Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)).substring(0, 4)
  } else {
    console.error('No shortener specified.')
    process.exit(1)
  }

  const configurationData = JSON.parse(
    await promisify(readFile)(configurationPath, 'utf8')
  )
  const site = configurationData.hosts[0]
  if (!site.redirects) {
    site.redirects = []
  }
  const from = new UriTemplate('/{name}').fill({ name })
  const to = new UriTemplate('/redirect/{?url}').fill({ url: options.url })
  site.redirects.push({ from, to })

  await promisify(writeFile)(
    configurationPath,
    JSON.stringify(configurationData, null, 2) + '\n'
  )

  let domain
  if (site.domain) {
    domain = site.domain
  } else {
    try {
      domain = (await promisify(readFile)('CNAME', 'utf8')).split('\n')[0].trim()
    } catch (error) {}
  }

  const origin = domain ? `https://${domainToUnicode(domain)}` : ''
  console.log(`🔗 ${origin}${decodeURI(from)}`)
}
